# [code-analytics-docs][documentation-home]

<a href="https://gitlab.com/software-productivity-analytics/code-analytics-docs/-/wikis/Home">
<img alt="graph"
    height="100" width="100"
    src="https://user-content.gitlab-static.net/29a6429647ad017829ba5d57b977f64d996b38a3/68747470733a2f2f63646e6a732e636c6f7564666c6172652e636f6d2f616a61782f6c6962732f6f637469636f6e732f382e332e302f7376672f67726170682e737667"></a>

In the book _Lean Analytics_, Croll and Yoskovitz define a "good metric" as something
that’s a) comparative, b) understandable, c) uses a ratio or a rate, and d) changes
the way you behave. [^1]

In order to "modernize" technology assets and promote an engineering culture, we
propose a data-driven approach that guides and informs us along our journey from
software fixes to innovation. We will use these tools and techniques:

-  The **"Goal/Question/Metrics" Framework** to operationalize how we communicate,
   prioritize, and track progress with data-driven insights;

-  The **AWS Well-Architected** to frame and improve our cloud infrastructure.

-  **Software Quality and Productivity** insights to identify challenges with and
   improvements for software development and delivery

-  **Layered visualizations** such as **badges** and **graphs** to help stakeholders
   understand the significance of data by placing it in a visual context.

---

_Learn more about [software quality and team productivity metrics....][documentation-home]_

---

## References

[^1]: Yoskovitz, B. & Croll, A. (2013). _Chapter 2. How to Keep Score_. In M., Tressler (Ed.). _Lean Analytics: Use Data to Build a Better Startup Faster_. (1st ed.) Retrieved 15 March 2019, from <https://learning.oreilly.com/library/view/lean-analytics/9781449335687/ch02.html#what_makes_a_good_metricquestion_mark>.

[documentation-home]: https://gitlab.com/software-productivity-analytics/code-analytics-docs/-/wikis/Home
